import static spark.Spark.*;

public class Hello {
    public static boolean killed = false;
    public static void main(String[] args) {
        get("/hello", (req, res) -> "Hello World");
        get("/helloAgain", (req, res) -> "Hello again");
        get("/notTested", (req, res) -> "notTested");
        get("/kill", (req, res) -> {
            killed = true;
            return "killed";
        });
    }
}
