import org.junit.Test;

public class IntegrationTest {

    @Test()
    public void RunNormally() {
        String[] args = new String[0];
        Hello.main(args);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Killing it ");
            Hello.killed = true;
        }));
        while (!Hello.killed) {
            try {
                 Thread.sleep(500);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
